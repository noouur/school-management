<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ReplyController;
use App\Http\Controllers\QuestionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Auth Routes
Route::post('/login', [UserController::class, 'login']);
Route::post('/register', [UserController::class, 'register']);

Route::middleware('auth:sanctum')->group(function() {

    Route::post('/logout', [UserController::class, 'logout']);

    // questions routes
    Route::post('/questions/{id}/replies', [ReplyController::class, 'store']);
    Route::delete('/questions/{id}/replies/{replyId}', [ReplyController::class, 'destory']);
    Route::post('/questions/{id}/replies/{replyId}', [ReplyController::class, 'rate']);
    Route::put('/questions/{id}/replies/{replyId}', [ReplyController::class, 'update']);
    Route::resource('/questions', QuestionController::class);

    // users routes
    Route::get('/users', [UserController::class, 'index']);
});
