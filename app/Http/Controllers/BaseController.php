<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{
    public function sendResponse($result,$message){
        $response = [
            'success'=> true,
            'data' => $result,
            'message' => $message,

        ];
        return response()->json($response,200);

    }
    public function sendError($erorr,$erorrMessage= [],$code = 404){
        $response = [
            'success'=> false,
            'data' => $erorr,
            
        ];
        if (!empty($erorrMessage)){
            $response['data'] = $erorrMessage;
        }

        return response()->json($response,$code);

    }
}
